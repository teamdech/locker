package teamdech.locker;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.common.Configuration;
import net.minecraftforge.common.MinecraftForge;
import teamdech.locker.config.ConfigHandler;
import teamdech.locker.event.InteractionHandler;
import teamdech.locker.player.PlayerHandler;

import java.io.File;
import java.util.ArrayList;

@Mod(name = "The Locker Mod", modid = LockerMod.MODID, version = LockerMod.VERSION)
public class LockerMod {
    public static boolean debug = false;

    public static final String MODID = "locker";
    public static final String VERSION = "1.0";

    public static Item locker;

    public static ArrayList<BlockLock> locked = new ArrayList<BlockLock>();

    @EventHandler
    public void preinit(FMLPreInitializationEvent event) {
        File config = event.getSuggestedConfigurationFile();
        Configuration cfg = new Configuration(config);
        new ConfigHandler(cfg);
    }

    @EventHandler
    public void init(FMLInitializationEvent event) {
        locker = new ItemLocker();
        GameRegistry.registerItem(locker, "ItemLocker");
        MinecraftForge.EVENT_BUS.register(new InteractionHandler());
        if (debug) {
            GameRegistry.registerPlayerTracker(new PlayerHandler());
        }
    }

    public static boolean canLeftClick(EntityPlayer p, int x, int y, int z) {
        boolean b = checkOpCreative(p) || !locked.contains(new BlockLock(x, y, z, BlockLock.LockType.LEFT_CLICK, ""));

        if (debug)
            System.out.printf("CAN_LEFT_CLICK: %d|%d|%d (%s) LIST CONTAINS: %s%n", x, y, z, b, locked.contains(new BlockLock(x, y, z, BlockLock.LockType.LEFT_CLICK, "")));

        return b;
    }

    public static boolean canRightClick(EntityPlayer p, int x, int y, int z) {
        boolean b = checkOpCreative(p) || !locked.contains(new BlockLock(x, y, z, BlockLock.LockType.RIGHT_CLICK, ""));

        if (debug)
            System.out.printf("CAN_RIGHT_CLICK: %d|%d|%d (%s) LIST CONTAINS: %s%n", x, y, z, b, locked.contains(new BlockLock(x, y, z, BlockLock.LockType.RIGHT_CLICK, "")));

        return b;
    }

    private static boolean checkOpCreative(EntityPlayer p) {
        boolean op = MinecraftServer.getServer().getConfigurationManager().isPlayerOpped(p.getEntityName());
        return op || p.capabilities.isCreativeMode;
    }
}
