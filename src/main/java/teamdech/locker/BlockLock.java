package teamdech.locker;

public class BlockLock {

    public final int x, y, z;
    public final LockType type;
    public final String owner;

    public BlockLock(int x, int y, int z, LockType type, String owner) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.type = type;
        this.owner = owner;
    }

    @Override
    public boolean equals(Object obj) {
        BlockLock theLock = (BlockLock) obj;

        if (LockerMod.debug) System.out.printf("Comparing %s to %s%n", theLock, this);

        return (theLock.x == x && theLock.y == y && theLock.z == z && (type == LockType.ALL || theLock.type == LockType.ALL || theLock.type == type));
    }

    @Override
    public String toString() {
        return String.format("BlockLock{%d,%d,%d|%s|%s}", x, y, z, type.name(), owner);
    }

    public enum LockType {
        ALL, LEFT_CLICK, RIGHT_CLICK
    }
}
