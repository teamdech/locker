package teamdech.locker;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ItemLocker extends Item {

    public static int id;

    public ItemLocker() {
        super(id);
        setUnlocalizedName("itemLocker");
    }


    @Override
    public CreativeTabs getCreativeTab() {
        return CreativeTabs.tabTools;
    }

    @Override
    public void registerIcons(IconRegister par1IconRegister) {
        par1IconRegister.registerIcon(LockerMod.MODID + ":stuff");
    }

    @Override
    public boolean onItemUseFirst(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ) {
        if (!LockerMod.locked.contains(new BlockLock(x, y, z, BlockLock.LockType.ALL, player.getEntityName()))) {

            LockerMod.locked.add(new BlockLock(x, y, z, BlockLock.LockType.ALL, player.getEntityName()));
        }
        return super.onItemUseFirst(stack, player, world, x, y, z, side, hitX, hitY, hitZ);
    }
}
