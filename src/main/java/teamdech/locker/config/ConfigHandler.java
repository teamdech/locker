package teamdech.locker.config;


import teamdech.locker.ItemLocker;
import net.minecraftforge.common.Configuration;

public class ConfigHandler {
    public ConfigHandler(Configuration config) {
        config.addCustomCategoryComment("Ids", "Item ID Configuration");
        ItemLocker.id = config.get("Ids", "itemLocker_id", 777).getInt(777);

        config.save();
    }
}
