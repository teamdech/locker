package teamdech.locker.player;

import cpw.mods.fml.common.IPlayerTracker;
import teamdech.locker.LockerMod;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

public class PlayerHandler implements IPlayerTracker {
    @Override
    public void onPlayerLogin(EntityPlayer player) {
        player.inventory.addItemStackToInventory(new ItemStack(LockerMod.locker));
    }

    @Override
    public void onPlayerLogout(EntityPlayer player) {
    }

    @Override
    public void onPlayerChangedDimension(EntityPlayer player) {
    }

    @Override
    public void onPlayerRespawn(EntityPlayer player) {
    }
}
