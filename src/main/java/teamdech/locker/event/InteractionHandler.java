package teamdech.locker.event;

import teamdech.locker.LockerMod;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;

public class InteractionHandler {

    @ForgeSubscribe
    public void onInteract(PlayerInteractEvent e) {
        double x, y, z;

        switch (e.action) {
            case RIGHT_CLICK_AIR:
                EntityPlayer p = e.entityPlayer;
                if (p == null) break;
                x = p.posX;
                y = p.posY;
                z = p.posZ;
                break;
            case RIGHT_CLICK_BLOCK:
                if (!LockerMod.canRightClick(e.entityPlayer, e.x, e.y, e.z))
                    e.setCanceled(true);
                break;
            case LEFT_CLICK_BLOCK:
                if (!LockerMod.canLeftClick(e.entityPlayer, e.x, e.y, e.z))
                    e.setCanceled(true);
                break;
        }
    }

}
